#include "../event.hpp"

#include <await/timers/impl/queue.hpp>
#include <await/executors/impl/fibers/pool.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/combine/seq/timeout.hpp>

#include <await/await.hpp>

#include <wheels/core/defer.hpp>

#include <iostream>
#include <thread>

using namespace await;
using namespace std::chrono_literals;

int main() {
  timers::Queue timers;
  executors::fibers::Pool pool{4};

  tutorial::AsyncEvent event;

  auto join = futures::Submit(pool, [&] {
    wheels::Defer note([] {
      std::cout << "Cancelled" << std::endl;
    });

    Await(event.Wait());
  }) | futures::WithTimeout(timers.Delay(1s));

  auto done = Await(std::move(join));

  assert(!done);  // Timeout

  pool.WaitIdle();
  pool.Stop();

  return 0;
}
