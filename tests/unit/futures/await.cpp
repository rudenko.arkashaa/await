#include <wheels/test/framework.hpp>

#include <await/tasks/exe/fibers/manual.hpp>

#include <await/await.hpp>

#include <await/futures/make/submit.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/bang.hpp>

// Internals
#include <await/fibers/core/suspend_guard.hpp>

using namespace await;

TEST_SUITE(FutureAwait) {
  SIMPLE_TEST(EagerFastPath) {
    return;  // TODO: fix optimization

    tasks::fibers::ManualExecutor manual;

    ~futures::Submit(manual, []() {
      auto f = !futures::Value(7);

      ASSERT_TRUE(f.HasOutput());

      {
        fibers::SuspendGuard guard;

        int value = Await(std::move(f));
        ASSERT_EQ(value, 7);

        ASSERT_FALSE(guard.HasBeenSuspended());
      }
    });

    manual.Drain();
  }
}