#include <await/tasks/exe/pools/fast/thread_pool.hpp>
#include <await/tasks/boot/submit.hpp>

#include <wheels/test/framework.hpp>

#include <twist/test/with/wheels/stress.hpp>

#include <twist/test/budget.hpp>
#include <twist/test/random.hpp>
#include <twist/test/repeat.hpp>

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/thread.hpp>

#include <atomic>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////

using await::tasks::pools::fast::ThreadPool;
using await::tasks::Submit;

////////////////////////////////////////////////////////////////////////////////

namespace tests {

void OneTask() {
  ThreadPool pool{4};

  while (twist::test::KeepRunning()) {
    size_t tasks = 0;

    Submit(pool, [&]() {
      ++tasks;
    });

    pool.WaitIdle();

    ASSERT_EQ(tasks, 1);
  }

  pool.Stop();
}

////////////////////////////////////////////////////////////////////////////////

void Series() {
  ThreadPool pool{1};

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    ++iter;
    const size_t tasks = 1 + iter % 3;

    size_t tasks_completed = 0;
    for (size_t i = 0; i < tasks; ++i) {
      Submit(pool, [&]() {
        ++tasks_completed;
      });
    }

    pool.WaitIdle();

    ASSERT_EQ(tasks_completed, tasks);
  }

  pool.Stop();
}

////////////////////////////////////////////////////////////////////////////////

void Current() {
  ThreadPool pool{2};

  while (twist::test::KeepRunning()) {
    bool done = false;

    Submit(pool, [&]() {
      Submit(*ThreadPool::Current(), [&]() {
        done = true;
      });
    });

    pool.WaitIdle();

    ASSERT_TRUE(done);
  }

  pool.Stop();
}

////////////////////////////////////////////////////////////////////////////////

void Concurrent() {
  ThreadPool pool{2};

  std::atomic<size_t> tasks = 0;

  twist::ed::stdlike::thread t1([&]() {
    Submit(pool, [&]() {
      ++tasks;
    });
  });

  twist::ed::stdlike::thread t2([&]() {
    pool.WaitIdle();
  });

  t1.join();
  t2.join();

  ASSERT_TRUE(tasks <= 1);

  pool.Stop();
}

////////////////////////////////////////////////////////////////////////////////

void Spawners() {
  ThreadPool pool{4};

  twist::test::Repeat repeat;

  while (repeat()) {
    std::atomic<size_t> tasks{0};
    std::atomic<size_t> done{0};

    size_t init = 1 + twist::test::Random(10);

    tasks += init;

    for (size_t i = 0; i < init; ++i) {
      size_t spawn;
      if (twist::test::Random2()) {
        spawn = 1 + twist::test::Random(100);
      } else {
        spawn = twist::test::Random(3);
      }

      tasks += spawn;

      Submit(pool, [&, spawn] {
        // Spawn tasks
        for (size_t j = 0; j < spawn; ++j) {
          Submit(pool, [&done] {
            ++done;
          });
        }

        ++done;
      });
    }

    size_t after = twist::test::Random(5);

    tasks += after;

    for (size_t k = 0; k < after; ++k) {
      Submit(pool, [&done] {
        ++done;
      });
    }

    pool.WaitIdle();

    ASSERT_EQ(tasks.load(), done.load());
  }

  std::cout << "Iterations: " << repeat.IterCount() << std::endl;

  pool.Stop();
}

}  // namespace tests

////////////////////////////////////////////////////////////////////////////////

TEST_SUITE(FastThreadPool) {
  TWIST_TEST(OneTask, 5s) {
    tests::OneTask();
  }

  TWIST_TEST(Series, 5s) {
    tests::Series();
  }

  TWIST_TEST(Current, 5s) {
    tests::Current();
  }

  TWIST_TEST_REPEAT(Concurrent, 5s) {
    tests::Concurrent();
  }

  TWIST_TEST_REPEAT(Spawners, 10s) {
    tests::Spawners();
  }
}

RUN_ALL_TESTS()
