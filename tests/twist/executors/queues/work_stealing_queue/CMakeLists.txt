ProjectLog("Stress tests for tasks::queues::WorkStealingQueue")

add_executable(await_stress_tests_wsqueue main.cpp)
target_link_libraries(await_stress_tests_wsqueue await)
