#pragma once

#include <await/await/awaiter.hpp>

namespace await {

//////////////////////////////////////////////////////////////////////

namespace ers {

// Add await::ers namespace to ADL
struct Lookup {};

}  // namespace ers

//////////////////////////////////////////////////////////////////////

template <typename A>
concept Awaitable = requires(A&& awaitable) {
  { GetAwaiter(std::move(awaitable), ers::Lookup{}) } -> Awaiter;
};

}  // namespace await
