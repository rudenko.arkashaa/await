#pragma once

#include <await/thread/awaiter.hpp>

#include <await/futures/types/future.hpp>

#include <await/cancel/never.hpp>
#include <await/tasks/core/cancelled.hpp>

#include <optional>

namespace await::ers {

//////////////////////////////////////////////////////////////////////

template <futures::SomeFuture Future>
class FutureAwaiter final : public thread::ISuspendingAwaiter,
                            public futures::IConsumer<typename Future::ValueType> {
 public:
  using ValueType = typename Future::ValueType;

 public:
  explicit FutureAwaiter(Future&& future)
      : future_(std::move(future)),
        cancel_token_(cancel::Never()) {
  }

  // Awaiter protocol

  bool AwaitReady() {
    return false;

    /*
    if constexpr (Future::IsLazy) {
      return false;
    } else {
      if (future_.HasResult()) {
        maybe_result_.emplace(std::move(future_).GetReadyResult());
        return true;
      }
      return false;
    }
    */
  }

  void AwaitSuspend(thread::Handle waiter) override {
    waiter_ = waiter;
    cancel_token_ = waiter.CancelToken();

    future_.Start(this);
  }

  ValueType AwaitResume() {
    if (HasOutput() && !cancel_token_.CancelRequested()) {
      return std::move(output_->value);
    } else {
      // Propagate cancellation
      throw tasks::CancelledException{};
    }
  }

  // futures::IConsumer<ValueType>

  cancel::Token CancelToken() override {
    return cancel_token_;
  }

  void Consume(futures::Output<ValueType> output) noexcept override {
    output_.emplace(std::move(output));
    ResumeWaiter(output_->context.exe.hint);
  }

  void Cancel(futures::Context) noexcept override {
    ResumeWaiter(tasks::SchedulerHint::UpToYou);  // <- Already has fired cancel token, so will throw
                     // CancelledException after resuming
  }

 private:
  bool HasOutput() const {
    return output_.has_value();
  }

  void ResumeWaiter(tasks::SchedulerHint hint) {
    assert(!waiter_.CancelToken().HasLinks());
    waiter_.Resume(hint);
  }

 private:
  Future future_;
  thread::Handle waiter_;
  cancel::Token cancel_token_;
  std::optional<futures::Output<ValueType>> output_;
};

}  // namespace await::ers
