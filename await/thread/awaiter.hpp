#pragma once

#include <await/thread/handle.hpp>

namespace await::thread {

//////////////////////////////////////////////////////////////////////

struct IAwaiter {
  virtual Handle AwaitSymmetricSuspend(Handle self) = 0;
};

//////////////////////////////////////////////////////////////////////

enum class AwaitEither : bool {
  Suspend = true,
  Resume = false
};

struct IMaybeSuspendingAwaiter : IAwaiter {
  virtual Handle AwaitSymmetricSuspend(Handle self) {
    switch (AwaitMaybeSuspend(self)) {
      case AwaitEither::Suspend:
        return Handle::Invalid();
      case AwaitEither::Resume:
        return self;  // Continue
    }
  }

  virtual AwaitEither AwaitMaybeSuspend(Handle self) = 0;
};

//////////////////////////////////////////////////////////////////////

struct ISuspendingAwaiter : IAwaiter {
  virtual Handle AwaitSymmetricSuspend(Handle self) {
    AwaitSuspend(self);
    return Handle::Invalid();  // Always suspend
  }

  virtual void AwaitSuspend(Handle self) = 0;
};

}  // namespace await::thread
