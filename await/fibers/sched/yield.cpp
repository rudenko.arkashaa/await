#include <await/fibers/sched/yield.hpp>

#include <await/tasks/curr/checkpoint.hpp>

#include <await/await.hpp>

#include <await/futures/make/just.hpp>
#include <await/futures/combine/seq/sched.hpp>

#include <await/thread/suspend.hpp>

#include <wheels/core/assert.hpp>

namespace await::fibers {

namespace {

struct YieldAwaiter : thread::ISuspendingAwaiter {
  void AwaitSuspend(thread::Handle h) override {
    WHEELS_ASSERT(h.IsFiber(), "Yield failed: not a Fiber");
    h.Resume(tasks::SchedulerHint::Yield);
  }
};

}  // namespace

void Yield() {
  tasks::curr::Checkpoint();

  YieldAwaiter awaiter;
  thread::Suspend(awaiter);

  tasks::curr::Checkpoint();
}

void YieldFuture() {
  Await(futures::Just() | futures::Yield());
}

}  // namespace await::fibers
