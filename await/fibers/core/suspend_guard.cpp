#include <await/fibers/core/suspend_guard.hpp>

#include <await/fibers/core/fiber.hpp>

namespace await::fibers {

SuspendGuard::SuspendGuard(fibers::Fiber& self)
    : self_(self),
      init_suspend_count_(self.GetSuspendCount()) {
}

SuspendGuard::SuspendGuard()
    : SuspendGuard(Fiber::Self()) {
}

bool SuspendGuard::HasBeenSuspended() {
  return self_.GetSuspendCount() > init_suspend_count_;
}

}  // namespace await::fibers
