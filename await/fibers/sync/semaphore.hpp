#pragma once

#include <await/thread/suspend.hpp>

#include <twist/ed/spin/lock.hpp>
#include <twist/ed/mutex/locker.hpp>

#include <wheels/intrusive/forward_list.hpp>

namespace await::fibers {

class Semaphore {
  using Locker = twist::ed::Locker<twist::ed::SpinLock>;

  class Waiter : public thread::ISuspendingAwaiter,
                 public wheels::IntrusiveForwardListNode<Waiter> {
   public:
    Waiter(Semaphore& host, Locker& l)
        : semaphore(host),
          locker(l) {
    }

    void AwaitSuspend(thread::Handle h) override {
      handle = h;
      semaphore.AddToWaitQueue(this);
    }

    Semaphore& semaphore;
    Locker& locker;
    thread::Handle handle;
  };

  friend class Waiter;

 public:
  explicit Semaphore(size_t permits)
      : permits_(permits) {
  }

  bool TryAcquire() {
    Locker locker{spinlock_};
    return TryAcquire(locker);
  }

  void Acquire() {
    Locker locker{spinlock_};

    if (TryAcquire(locker)) {
      return;  // Fast path
    }

    // Slow path
    Waiter waiter{*this, locker};
    thread::Suspend(waiter);
  }

  void Release() {
    Locker locker{spinlock_};

    if (!waiters_.IsEmpty()) {
      Waiter* waiter = waiters_.PopFront();
      locker.Unlock();
      ResumeWaiter(waiter->handle);
    } else {
      ++permits_;
    }
  }

 private:
  bool TryAcquire(Locker&) {
    if (permits_ > 0) {
      --permits_;
      return true;
    } else {
      return false;
    }
  }

  void AddToWaitQueue(Waiter* waiter) {
    waiters_.PushBack(waiter);
    waiter->locker.Unlock();
  }

  static void ResumeWaiter(thread::Handle fiber) {
    fiber.Resume(tasks::SchedulerHint::Next);
  }

 private:
  twist::ed::SpinLock spinlock_;
  size_t permits_;
  wheels::IntrusiveForwardList<Waiter> waiters_;
};

}  // namespace await::fibers
