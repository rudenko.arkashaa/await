#include <await/fibers/boot/impl/go.hpp>

#include <await/fibers/core/fiber.hpp>
#include <await/fibers/core/resource_manager.hpp>

namespace await::fibers::boot {

//////////////////////////////////////////////////////////////////////

Fiber* CreateFiber(IRunnable* runnable, IResourceManager& manager,
                   tasks::IExecutor& executor) {
  // Allocate resources
  auto id = manager.GenerateId();
  auto stack = manager.AllocateStack();

  Fiber* f = new Fiber(runnable, stack, manager, executor, id);

  return f;
}

//////////////////////////////////////////////////////////////////////

void Go(IRunnable* runnable) {
  Fiber& caller = Fiber::Self();

  Fiber* f = CreateFiber(runnable,
                         caller.GetManager(),    // Inherit from caller
                         caller.GetExecutor());  // Inherit from caller

  f->Schedule(tasks::SchedulerHint::New);
}

void Go(tasks::IExecutor& where, IRunnable* runnable) {
  auto* f = CreateFiber(runnable, GlobalResourceManager(), where);

  f->Schedule(tasks::SchedulerHint::New);
}

}  // namespace await::fibers::boot
