#pragma once

#include <await/tasks/core/executor.hpp>
#include <await/tasks/core/task.hpp>

#include <await/tasks/exe/pools/fast/scheduler.hpp>
#include <await/tasks/exe/pools/fast/runner.hpp>

#include <await/tasks/exe/pools/fast/metrics.hpp>
#include <await/tasks/exe/pools/fast/queues/work_stealing_queue.hpp>

#include <await/thread/system.hpp>

#include <twist/ed/stdlike/thread.hpp>

#include <wheels/intrusive/list.hpp>

#include <optional>
#include <random>

namespace await::tasks::pools::fast {

class ThreadPool;

// Worker = Scheduler + Runner

class Worker : public wheels::IntrusiveListNode<Worker>,
               public thread::SystemThread,
               private IScheduler {
  friend class ThreadPool;

 public:
  Worker(ThreadPool& host, size_t index);

  void Start();
  void Join();

  // Single producer
  void LocalPush(TaskBase* task, SchedulerHint hint);

  // Steal from this worker
  size_t StealTasks(std::span<TaskBase*> out_buffer);

  // Wake parked worker
  void Wake();

  static Worker* Current();

  WorkerMetrics Metrics() const {
    return metrics_;
  }

  // Always valid
  ThreadPool* Host() const {
    return &host_;
  }

 private:
  void PushToLifoSlot(TaskBase* task);
  void PushToLocalQueue(TaskBase* task);
  void OffloadTasksToGlobalQueue(TaskBase* overflow);

  TaskBase* TryPickTask();
  TaskBase* TryPickTaskFromLifoSlot();
  TaskBase* TryStealTasks(size_t series);
  TaskBase* GrabTasksFromGlobalQueue();
  TaskBase* TryPickTaskFromGlobalQueue();
  TaskBase* TryPickTaskBeforePark();

  // IScheduler

  bool StopRequested() const override;
  TaskBase* PickTask() override;

  void Stop();

  void Work();

 private:
  ThreadPool& host_;
  const size_t index_;

  std::optional<twist::ed::stdlike::thread> thread_;

  // Scheduling iteration
  size_t iter_ = 0;

  // Lifo scheduling
  TaskBase* lifo_slot_{nullptr};
  size_t lifo_steps_ = 0;

#if defined(__TWIST_FAULTY__)
  static constexpr const size_t kLocalQueueCapacity = 17;
#else
  static constexpr const size_t kLocalQueueCapacity = PerformantLocalQueueCapacity(255);
#endif

  using LocalTaskQueue = WorkStealingQueue<TaskBase, kLocalQueueCapacity>;
  LocalTaskQueue local_tasks_;

  // For work stealing
  std::mt19937_64 twister_;

  // Transfer buffer for grabbing from global queue
  // and for stealing from other workers
  TaskBase* buffer_[kLocalQueueCapacity];

  WorkerMetrics metrics_;
};

}  // namespace await::tasks::pools::fast
