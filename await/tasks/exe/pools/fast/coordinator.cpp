#include <await/tasks/exe/pools/fast/coordinator.hpp>

#include <await/tasks/exe/pools/fast/worker.hpp>

namespace await::tasks::pools::fast {

//////////////////////////////////////////////////////////////////////

struct StateView {
  uint32_t idle;
  uint32_t spinning;
};

StateView View(uint64_t state) {
  return {
      (uint32_t)(state >> 32),
      (uint32_t)(state & (((uint64_t)1 << 32) - 1))
  };
}

//////////////////////////////////////////////////////////////////////

static uint64_t AddSpinner(uint64_t state) {
  return state + 1;
}

bool Coordinator::StartSpinning() {
  uint64_t curr = state_.load();

  while (true) {
    auto view = View(curr);

    if ((view.spinning + 1) * 2 > threads_) {
      return false;
    }

    uint64_t next = AddSpinner(curr);

    if (state_.compare_exchange_weak(curr, next)) {
      return true;
    }
  }
}

bool Coordinator::StopSpinning() {
  auto old = state_.fetch_sub(1);
  auto view = View(old);
  return view.spinning == 1;  // Last spinning worker
}

bool Coordinator::ShouldWakeWorker() const {
  auto curr = state_.load();
  auto view = View(curr);

  // 1) Idle threads > 0
  // 2) Number of spinning (stealing) threads == 0
  return (view.idle > 0) && (view.spinning == 0);
}

static const uint64_t kIdleWorker = (uint64_t)1 << 32;

void Coordinator::IncrIdleCount() {
  state_.fetch_add(kIdleWorker);
}

void Coordinator::DecrIdleCount() {
  state_.fetch_sub(kIdleWorker);
}

void Coordinator::StepDownAsActiveWorker(Worker* worker) {
  std::lock_guard locker(mutex_);

  idle_.PushBack(worker);
  IncrIdleCount();
}

void Coordinator::BecomeActiveWorker(Worker* worker) {
  std::lock_guard locker(mutex_);

  if (worker->IsLinked()) {
    worker->Unlink();
    DecrIdleCount();
  }
}

void Coordinator::WakeWorker() {
  std::lock_guard locker(mutex_);

  if (Worker* worker = idle_.PopBack()) {
    DecrIdleCount();
    worker->Wake();
  }
}

void Coordinator::WakeAll() {
  std::lock_guard locker(mutex_);

  while (idle_.NonEmpty()) {
    DecrIdleCount();
    Worker* worker = idle_.PopBack();
    worker->Wake();
  }
}

}  // namespace await::tasks::pools::fast
