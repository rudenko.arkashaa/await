#pragma once

#include <await/tasks/core/task.hpp>

#include <wheels/intrusive/forward_list.hpp>

#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/spin/lock.hpp>

#include <algorithm>
#include <span>

namespace await::tasks::pools::fast {

template <typename T>
class GlobalQueue {
  using List = wheels::IntrusiveForwardList<T>;

  // using Mutex = twist::ed::stdlike::mutex;
  using Mutex = twist::ed::SpinLock;

 public:
  void PushOne(T* item) {
    std::lock_guard locker(mutex_);
    items_.PushBack(item);
  }

  void Offload(std::span<T*> buffer) {
    List as_list;
    for (size_t i = 0; i < buffer.size(); ++i) {
      as_list.PushBack(buffer[i]);
    }

    {
      std::lock_guard locker(mutex_);
      items_.Append(as_list);  // O(1)
    }
  }

  T* TryPopOne() {
    std::lock_guard locker(mutex_);
    return items_.PopFront();
  }

  size_t Grab(std::span<T*> buffer, size_t workers) {
    std::lock_guard locker(mutex_);

    size_t size = items_.Size();

    if (size == 0) {
      return 0;
    }

    size_t share = std::max<size_t>(size / workers, 1);

    size_t to_grab = std::min(buffer.size(), share);

    size_t count = 0;
    while (to_grab > 0) {
      T* front = items_.PopFront();
      if (front == nullptr) {
        break;
      }
      buffer[count++] = front;
      --to_grab;
    }
    return count;
  }

 private:
  List items_;
  Mutex mutex_;
};

}  // namespace await::tasks::pools::fast
