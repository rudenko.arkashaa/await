#include <await/tasks/exe/fibers/pool/runner.hpp>

#include <await/fibers/core/fiber.hpp>
#include <await/fibers/core/resource_manager.hpp>
#include <await/fibers/core/suspend_guard.hpp>

#include <await/thread/suspend.hpp>

#include <await/tasks/exe/prison.hpp>

#include <twist/ed/local/ptr.hpp>

#include <cassert>

namespace await::tasks::runners {

//////////////////////////////////////////////////////////////////////

TWISTED_THREAD_LOCAL_PTR(ShadowWorker, this_shadow_worker)

//////////////////////////////////////////////////////////////////////

void IdleCarrier::AwaitSuspend(thread::Handle f) {
  fiber_ = f.GetFiber();
  this_shadow_worker->AddToPool(/*carrier=*/this);
}

void IdleCarrier::DoResume(IScheduler* scheduler) {
  scheduler_ = scheduler;
  fiber_->Run();
}

//////////////////////////////////////////////////////////////////////

void ShadowWorker::Run() {
  while (!scheduler_.StopRequested()) {
    IdleCarrier* carrier = TakeIdleCarrier();

    // Run tasks from scheduler
    carrier->Resume(scheduler_);
  }

  Stop();
}

void ShadowWorker::Stop() {
  while (carriers_.NonEmpty()) {
    carriers_.PopFront()->Stop();
  }
}

void ShadowWorker::AddToPool(IdleCarrier* carrier) {
  carriers_.PushBack(carrier);
}

IScheduler* ShadowWorker::AcquireScheduler(fibers::Fiber* self) {
  IdleCarrier carrier;
  self->Suspend(&carrier);
  return carrier.GetScheduler();
}

IdleCarrier* ShadowWorker::TakeIdleCarrier() {
  if (carriers_.IsEmpty()) {
    StartNewCarrier();
  }
  assert(carriers_.NonEmpty());
  return carriers_.PopFront();
}

void ShadowWorker::StartNewCarrier() {
  // Allocate resources

  auto stack = host_.ResourceManager().AllocateStack();
  auto id = host_.ResourceManager().GenerateId();

  // Create fiber

  // clang-format off

  auto* carrier = new fibers::Fiber(
      /*runnable=*/this,
      std::move(stack),
      host_.ResourceManager(),
      tasks::Prison(),  // carrier should not run by itself
      id);

  // clang-format on

  // First step

  // Stops at first AcquireScheduler and goes to
  // pool of this ShadowWorker
  carrier->Run();
}

// Carrier routine
void ShadowWorker::RunCarrier(fibers::Fiber* self) noexcept {
  // Put self to current ShadowWorker's pool of free carriers
  while (IScheduler* scheduler = AcquireScheduler(self)) {
    self->SetName("Carrier");

    // Resumed by some shadow worker

    while (TaskBase* next = scheduler->PickTask()) {
      fibers::SuspendGuard guard{*self};

      next->Run();

      bool detached = guard.HasBeenSuspended();

      if (detached) {
        break;  // Promoted to a user task, detached from worker
      }
    }
  }

  // <- Stopped by shadow worker
}

void ShadowWorker::RunCoro(void* bootstrap) noexcept {
  RunCarrier((fibers::Fiber*)bootstrap);
}

//////////////////////////////////////////////////////////////////////

FiberTaskRunner::FiberTaskRunner()
    : FiberTaskRunner(fibers::GlobalResourceManager()) {
}

void FiberTaskRunner::RunWorker(IScheduler& scheduler) {
  ShadowWorker shadow_worker{*this, scheduler};

  this_shadow_worker = &shadow_worker;

  shadow_worker.Run();
}

}  // namespace await::tasks::runners
