#include <await/tasks/exe/inline.hpp>

namespace await::tasks {

//////////////////////////////////////////////////////////////////////

// [Await.Executor]
class InlineExecutor final : public IExecutor {
 public:
  // IExecutor
  void Submit(TaskBase* task, SchedulerHint) override {
    task->Run();
  }
};

//////////////////////////////////////////////////////////////////////

IExecutor& Inline() {
  static InlineExecutor instance;
  return instance;
}

};  // namespace await::tasks
