#pragma once

#include <await/tasks/exe/pools/compute/thread_pool.hpp>
#include <await/tasks/exe/pools/fast/thread_pool.hpp>

namespace await::tasks {

// Default thread pool
using ThreadPool = pools::fast::ThreadPool;

}  // namespace await::tasks
