#pragma once

#include <await/tasks/core/task.hpp>
#include <await/tasks/core/hint.hpp>

namespace await::tasks {

// Executors are to function execution as allocators are to memory allocation

struct IExecutor {
  virtual ~IExecutor() = default;

  // Submit task for an execution
  //
  // Low-level, do not use directly,
  // use futures API to launch tasks

  // Memory management:
  // Task pointer is actually a holding reference
  // that is implicitly released by Run operation

  virtual void Submit(TaskBase* task, SchedulerHint hint) = 0;

  // TODO: SubmitMany
};

}  // namespace await::tasks
