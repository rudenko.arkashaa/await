#pragma once

#include <refer/ref.hpp>

#include <wheels/intrusive/forward_list.hpp>

namespace await::cancel {

//////////////////////////////////////////////////////////////////////

class Signal {
  enum class Either {
    Cancel,
    Release,
  };

 public:
  static Signal Cancel() {
    return Signal{Either::Cancel};
  }

  static Signal Release() {
    return Signal{Either::Release};
  }

  bool CancelRequested() const {
    return either_ == Either::Cancel;
  }

  bool Released() const {
    return either_ == Either::Release;
  }

 private:
  Signal(Either either)
      : either_(either) {
  }

 private:
  Either either_;
};

//////////////////////////////////////////////////////////////////////

struct IHandler {
  virtual ~IHandler() = default;

  // NB: release-reference operation
  virtual void Forward(Signal signal) = 0;
};

struct HandlerBase : IHandler,
                     wheels::IntrusiveForwardListNode<HandlerBase> {
  //
};

//////////////////////////////////////////////////////////////////////

namespace detail {

struct StateBase : HandlerBase,
                   virtual refer::IManaged {
  virtual bool Cancellable() const = 0;

  // Send cancellation request

  virtual void RequestCancel() = 0;

  // Pollable

  virtual bool CancelRequested() const = 0;

  // Handlers

  // NB: Owning reference
  // Returns true if linked, false if released / cancelled immediately
  virtual bool AddHandler(HandlerBase* handler) = 0;

  // NB: Non-owning reference
  virtual void RemoveHandler(HandlerBase* handler) = 0;

  // Debugging
  virtual bool HasHandlers() const = 0;
};

//////////////////////////////////////////////////////////////////////

using StateRef = refer::Ref<StateBase>;

}  // namespace detail

}  // namespace await::cancel
