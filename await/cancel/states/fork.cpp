#include <await/cancel/states/fork.hpp>

namespace await::cancel {

namespace detail {

static void ForwardToHandlers(HandlerBase* head, Signal signal) {
  HandlerBase* curr = head;

  while (curr != nullptr) {
    auto* next = (HandlerBase*)curr->Next();
    curr->Forward(signal);
    curr = next;
  }
}

ForkStateBase::~ForkStateBase() {
  assert(!HasHandlers());
}

// Consumer

bool ForkStateBase::AddHandler(HandlerBase* handler) {
  while (true) {
    State curr = state_.load(std::memory_order::acquire);

    // Too late
    if (IsCancelRequested(curr)) {
      handler->Forward(Signal::Cancel());
      return false;  // Cancelled
    }

    // Handlers or Initial

    if (curr.IsPointer()) {
      // Handlers
      handler->next_ = curr.AsPointerTo<HandlerBase>();
    } else {
      // Initial
      handler->next_ = nullptr;
    }

    if (state_.compare_exchange_weak(curr, State::Pointer(handler),
                                     std::memory_order::release,
                                     std::memory_order::relaxed)) {
      return true;  // Linked
    }
  }
}

void ForkStateBase::RemoveHandler(HandlerBase* /*handler*/) {
  // Not implemented
  // ReleaseHandlers required
}

void ForkStateBase::ReleaseHandlers() {
  while (true) {
    State curr = state_.load(std::memory_order::acquire);

    if (IsCancelRequested(curr)) {
      return;  // Terminal state
    }

    if (IsInitial(curr)) {
      return;  // Nothing to do
    }

    // Handlers
    if (state_.compare_exchange_weak(curr, State::Value(kInitial),
                                     std::memory_order::release,
                                     std::memory_order::relaxed)) {
      HandlerBase* head = curr.AsPointerTo<HandlerBase>();

      // Release handlers
      ForwardToHandlers(head, Signal::Release());

      return;
    }
  }
}

bool ForkStateBase::HasHandlers() const {
  return state_.load().IsPointer();
}

bool ForkStateBase::CancelRequested() const {
  return IsCancelRequested(state_.load(std::memory_order::acquire));
}

// Single producer

void ForkStateBase::Forward(Signal signal) {
  if (signal.CancelRequested()) {
    RequestCancel();
  } else {
    // Ignore
  }

  // Memory management!
  ReleaseRef();
}

void ForkStateBase::RequestCancel() {
  // Switch to terminal state
  State prev = state_.exchange(State::Value(kCancelRequested),
                               std::memory_order::acq_rel);

  if (prev.IsPointer()) {
    HandlerBase* head = prev.AsPointerTo<HandlerBase>();

    // Propagate cancel signal to handlers
    ForwardToHandlers(head, Signal::Cancel());
  }
}

}  // namespace detail

}  // namespace await::cancel
