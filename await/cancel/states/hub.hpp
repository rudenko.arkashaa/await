#pragma once

#include <await/cancel/states/fork.hpp>

namespace await::cancel {

namespace detail {

// Many inputs, many outputs
// TODO: Better impl

class HubStateBase : public ForkStateBase {
 public:
  ~HubStateBase();

  // Wait-free
  void Forward(Signal) override;
};

}  // namespace detail

}  // namespace await::cancel
