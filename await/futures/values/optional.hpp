#pragma once

// Customization points

#include <await/futures/impl/value/optional/to_unit.hpp>

#include <await/futures/impl/value/optional/is_monad.hpp>
#include <await/futures/impl/value/optional/map.hpp>
#include <await/futures/impl/value/optional/and_then.hpp>
#include <await/futures/impl/value/optional/or_else.hpp>

#include <await/futures/impl/value/optional/result.hpp>

#include <await/futures/impl/value/optional/all.hpp>
#include <await/futures/impl/value/optional/join.hpp>
