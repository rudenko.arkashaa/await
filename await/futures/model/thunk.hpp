#pragma once

#include <await/futures/model/consumer.hpp>

namespace await::futures::lazy {

// https://wiki.haskell.org/Thunk

// A thunk is an object that represents a value
// that is yet to be evaluated

// Evaluation is only triggered by explicit consume operation (e.g. Await)

// Consume operation is represented by IConsumer interface

// clang-format off

template <typename T>
concept Thunk = requires (T thunk, IConsumer<typename T::ValueType>* consumer) {
  typename T::ValueType;

  // Start evaluation
  thunk.Start(consumer);
};

// clang-format on

}  // namespace await::futures::lazy
