#pragma once

#include <await/futures/model/output.hpp>

#include <await/cancel/token.hpp>

namespace await::futures {

// Represents future combinator, suspended fiber or blocked thread

template <typename T>
struct IConsumer {
  virtual ~IConsumer() = default;

  // Producer -> Consumer

  // [Dev] Consume/Cancel are release-reference operations!

  virtual void Consume(Output<T>) noexcept = 0;

  virtual void Consume(T value) noexcept {
    Consume({std::move(value), Context{}});
  }

  virtual void Cancel(Context) noexcept = 0;

  // Consumer -> Producer

  virtual cancel::Token CancelToken() = 0;
};

}  // namespace await::futures
