#pragma once

#include <await/futures/model/context.hpp>

namespace await::futures {

// Output = T + Context

template <typename T>
struct Output {
  T value;
  Context context;
};

}  // namespace await::futures
