#pragma once

#include <await/futures/types/eager.hpp>

#include <await/futures/impl/eager/states/contract.hpp>
#include <await/futures/impl/eager/promise.hpp>

#include <await/futures/impl/lazy/thunks/seq/eager.hpp>

#include <tuple>

namespace await::futures {

/*
 * Example:
 *
 * auto [future, promise] = futures::Contract<T>();
 * // https://en.cppreference.com/w/cpp/language/structured_binding
 *
 * // Producer
 * std::move(promise).Set(value)
 *
 */

using eager::Promise;

template <typename T>
std::tuple<EagerFuture<T>, Promise<T>> Contract() {
  auto shared_state = refer::New<eager::states::Contract<T>>();

  return {
      lazy::thunks::Eager<T>{shared_state},
      Promise<T>(shared_state)
  };
}

}  // namespace await::futures
