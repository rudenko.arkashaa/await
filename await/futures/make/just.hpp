#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/make/just.hpp>

namespace await::futures {

/*
 * Represents ready Unit value
 *
 * Idiomatic way to schedule user task in Await:
 *
 * auto task = Just()
 *             | Via(executor)
 *             | With(context)
 *             | Apply([] {
 *                 fmt::println("Running on executor");
 *               });
 */

inline UnitFuture auto Just() {
  return lazy::thunks::JustUnit{};
}

}  // namespace await::futures
