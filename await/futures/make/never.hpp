#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/make/never.hpp>

namespace await::futures {

/*
 * Unit value that will never be evaluated
 *
 * Idiomatic (and optimal =) way to block current thread or fiber forever:
 * {fibers, threads}::Await(futures::Never()).ExpectOk();
 *
 */

inline UnitFuture auto Never() {
  return lazy::thunks::Never{};
}

}  // namespace await::futures
