#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/values/expected.hpp>

namespace await::futures {

template <typename F>
concept SomeTryFuture = SomeFuture<F> && value::IsExpected<typename F::ValueType>;

template <typename F, typename T, typename E>
concept TryFuture = Future<F, tl::expected<T, E>>;

}  // namespace await::futures
