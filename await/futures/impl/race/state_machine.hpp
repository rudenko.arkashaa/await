#pragma once

#include <twist/ed/stdlike/atomic.hpp>

#include <wheels/core/assert.hpp>

namespace await::futures {

namespace race {

//////////////////////////////////////////////////////////////////////

// Wait-free

class RendezvousStateMachine {
 private:
  struct States {
    enum _ {
      Initial = 0,
      Consumer = 1,
      Producer = 2,
      Rendezvous = Producer | Consumer
    };
  };

  using State = int64_t;

 public:
  bool Produce() {
    switch (state_.fetch_or(States::Producer, std::memory_order::acq_rel)) {
      case States::Initial:
        // Initial -> Producer
        return false;
      case States::Consumer:
        // Consumer -> Rendezvous
        return true;
      default:
        WHEELS_PANIC("Unexpected rendezvous state");
    }
  }

  bool Consume() {
    switch (state_.fetch_or(States::Consumer, std::memory_order::acq_rel)) {
      case States::Initial:
        // Initial -> Consumer
        return false;
      case States::Producer:
        // Producer -> Rendezvous
        return true;
      default:
        WHEELS_PANIC("Unexpected rendezvous state");
    }
  }

  bool Produced() const {
    return state_.load(std::memory_order::acquire) == States::Producer;
  }

 private:
  twist::ed::stdlike::atomic<State> state_{States::Initial};
};

}  // namespace race

}  // namespace await::futures
