#pragma once

#include <await/futures/impl/race/state_machine.hpp>
#include <await/futures/impl/race/ready.hpp>

#include <await/futures/impl/consumers/linked.hpp>

#include <await/cancel/token.hpp>

#include <optional>
#include <variant>

namespace await::futures {

namespace race {

//////////////////////////////////////////////////////////////////////

// Producer: SetOutput / SetCancel
// Consumer: SetConsumer
// Progress guarantee: Wait-freedom

template <typename T>
class Rendezvous {
 public:
  bool SetConsumer(consumers::LinkedConsumer<T>&& consumer) {
    consumer_ = std::move(consumer);

    if (rendezvous_.Consume()) {
      MakeRendezvous();
      return true;
    } else {
      return false;
    }
  }

  Ready GetState() const {
    if (!rendezvous_.Produced()) {
      return Ready::NotReady;
    }

    // Produced
    if (HasOutput()) {
      return Ready::HasOutput;
    } else {
      return Ready::Cancelled;
    }
  }

  // Producer

  void SetOutput(Output<T> output) {
    output_.template emplace<1>(std::move(output));
    DoProduce();
  }

  void SetCancel(Context context) {
    output_.template emplace<2>(std::move(context));
    DoProduce();
  }

 private:
  void DoProduce() {
    if (rendezvous_.Produce()) {
      MakeRendezvous();
    }
  }

  bool HasOutput() const {
    return output_.index() == 1;
  }

  void MakeRendezvous() {
    if (HasOutput()) {
      // Consume produced output
      consumer_->Consume(std::move(std::get<1>(output_)));
    } else {
      // Cancel consumer
      consumer_->Cancel(std::move(std::get<2>(output_)));
    }
  }

 private:
  RendezvousStateMachine rendezvous_;
  std::variant<std::monostate, Output<T>, Context> output_;
  consumers::LinkedConsumer<T> consumer_;
};

}  // namespace race

}  // namespace await::futures
