#pragma once

#include <await/futures/impl/value/t/and_then.hpp>

#include <await/futures/impl/value/optional/is.hpp>

#include <optional>
#include <type_traits>

namespace await::futures::value {

namespace apply {

template <typename T, typename F>
struct AndThen<std::optional<T>, F> {
  F f;

  // Mapper output type
  using U = std::invoke_result_t<F, T>;

  // Requirements for mapper output type
  static_assert(IsStdOptional<U>);

  auto operator()(std::optional<T> o) {
    if (o) {
      return f(std::move(*o));
    } else {
      return std::nullopt;
    }
  }
};

}  // namespace apply

}  // namespace await::futures::value
