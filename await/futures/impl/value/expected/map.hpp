#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/value/t/map.hpp>
#include <await/futures/impl/value/expected/is.hpp>

#include <type_traits>

namespace await::futures::value {

namespace apply {

// expected<T, E> -> (T -> U) -> std::expected<U, E>

template <typename T, typename E, typename F>
struct Map<tl::expected<T, E>, F> {
  F f;

  // Mapper output type
  using U = std::invoke_result_t<F, T>;

  // Requirements for mapper output type
  static_assert(!IsExpected<U> && !lazy::Thunk<U>);

  auto operator()(tl::expected<T, E> e) -> tl::expected<U, E> {
    return e.map(std::move(f));
  };
};

}  // namespace apply

}  // namespace await::futures::value
