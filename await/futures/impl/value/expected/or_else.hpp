#pragma once

#include <await/futures/impl/value/t/or_else.hpp>

#include <tl/expected.hpp>

#include <type_traits>

namespace await::futures::value {

namespace apply {

// expected<T, E> -> (E -> expected<T, E>) -> expected<T, E>

template <typename T, typename E, typename F>
struct OrElse<tl::expected<T, E>, F> {
  F f;

  // Mapper output type
  using R = std::invoke_result_t<F, E>;

  // Requirements for mapper output type
  static_assert(std::is_same_v<R, tl::expected<T, E>>);

  auto operator()(tl::expected<T, E> e) -> tl::expected<T, E> {
    return e.or_else(std::move(f));
  };
};

}  // namespace apply

}  // namespace await::futures::value
