#pragma once

#include <await/futures/impl/value/t/to_unit.hpp>

#include <wheels/core/unit.hpp>

#include <tl/expected.hpp>

namespace await::futures::value {

template <typename T, typename E>
struct ToUnitTraits<tl::expected<T, E>> {
  using UnitType = tl::expected<wheels::Unit, E>;

  static UnitType ToUnit(tl::expected<T, E> e) {
    return e.map([](T) {
      return wheels::Unit{};
    });
  }
};

}  // namespace await::futures::value
