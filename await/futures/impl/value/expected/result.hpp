#pragma once

#include <await/futures/impl/value/t/result.hpp>

#include <tl/expected.hpp>

namespace await::futures::value {

template <typename T, typename E>
struct ResultTraits<tl::expected<T, E>> {
  static bool IsOk(tl::expected<T, E>& e) {
    return e.has_value();
  }
};

}  // namespace await::futures::value
