#pragma once

#include <await/futures/impl/value/t/all.hpp>

#include <tl/expected.hpp>

namespace await::futures::value {

template <typename T, typename E>
struct AllTraits<tl::expected<T, E>> {
  using VectorType = std::vector<T>;
  using OutputType = tl::expected<VectorType, E>;

  static bool IsOk(const tl::expected<T, E>& e) {
    return e.has_value();
  }

  static T UnwrapValue(tl::expected<T, E> e) {
    return std::move(*e);
  }

  static OutputType WrapValues(VectorType vs) {
    return std::move(vs);
  }

  static OutputType PropagateError(tl::expected<T, E> e) {
    return e.error();
  }
};

}  // namespace await::futures::value
