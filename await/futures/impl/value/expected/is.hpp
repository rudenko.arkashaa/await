#pragma once

#include <tl/expected.hpp>

namespace await::futures::value {

namespace match {

template <typename T>
struct IsExpected {
  static const bool Value = false;
};

template <typename T, typename E>
struct IsExpected<tl::expected<T, E>> {
  static const bool Value = true;
};

}  // namespace match

template <typename T>
concept IsExpected = match::IsExpected<T>::Value;

}  // namespace await::futures::value
