#pragma once

#include <type_traits>

namespace await::futures::value {

namespace match {

template <typename T>
struct IsMonad : std::false_type {};

}  // namespace match

template <typename T>
concept IsMonad = match::IsMonad<T>::value;

}  // namespace await::futures::value
