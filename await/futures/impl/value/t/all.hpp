#pragma once

#include <vector>

namespace await::futures::value {

template <typename InputType>
struct AllTraits {
  using VectorType = std::vector<InputType>;
  using OutputType = VectorType;

  static bool IsOk(const InputType&) {
    return true;
  }

  static InputType UnwrapValue(InputType v) {
    return std::move(v);
  }

  static OutputType WrapValues(VectorType vs) {
    return std::move(vs);
  }

  static OutputType PropagateError(InputType) {
    std::abort();  // Not used
  }
};

}  // namespace await::futures::value
