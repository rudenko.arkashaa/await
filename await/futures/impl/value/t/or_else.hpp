#pragma once

namespace await::futures::value {

namespace apply {

template <typename T, typename F>
struct OrElse {
  // Not supported, monadic T required
};

}  // namespace apply

}  // namespace await::futures::value
