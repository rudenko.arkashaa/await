#pragma once

#include <await/futures/model/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

// Scheduling hint

template <Thunk Wrapped>
class [[nodiscard]] Sched final
    : private IConsumer<typename Wrapped::ValueType> {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  Sched(Wrapped&& thunk, tasks::SchedulerHint hint)
      : wrapped_(std::move(thunk)), hint_(hint) {
  }

  // Non-copyable
  Sched(const Sched&) = delete;

  // Movable
  Sched(Sched&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    wrapped_.Start(this);
  }

 private:
  // IConsumer

  void Consume(Output<ValueType> out) noexcept override {
    out.context.exe.hint = hint_;
    consumer_->Consume(std::move(out));
  }

  void Cancel(Context ctx) noexcept override {
    consumer_->Cancel(std::move(ctx));
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  Wrapped wrapped_;
  tasks::SchedulerHint hint_;

  IConsumer<ValueType>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy
