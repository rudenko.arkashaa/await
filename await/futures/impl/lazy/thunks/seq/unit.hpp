#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/value/t/to_unit.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] ToUnit final : private IConsumer<typename Wrapped::ValueType>{
  using InputValueType = typename Wrapped::ValueType;

 public:
  using ToUnitTraits = value::ToUnitTraits<InputValueType>;
  using ValueType = typename ToUnitTraits::UnitType;

 public:
  ToUnit(Wrapped&& thunk)
      : wrapped_(std::move(thunk)) {
  }

  // Non-copyable
  ToUnit(const ToUnit&) = delete;

  // Movable
  ToUnit(ToUnit&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    wrapped_.Start(this);
  }

 private:
  // IConsumer<InputValueType>

  void Consume(Output<InputValueType> output) noexcept override {
    consumer_->Consume({
        ToUnitTraits::ToUnit(std::move(output.value)), std::move(output.context)});
  }

  void Cancel(Context ctx) noexcept override {
    consumer_->Cancel(std::move(ctx));
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  Wrapped wrapped_;
  IConsumer<ValueType>* consumer_ = nullptr;
};

}  // namespace thunks

}  // namespace await::futures::lazy
