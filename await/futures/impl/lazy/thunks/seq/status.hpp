#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/value/t/result.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] ToStatus final : private IConsumer<typename Wrapped::ValueType> {
  using InputValueType = typename Wrapped::ValueType;

 public:
  using ResultTraits = value::ResultTraits<InputValueType>;
  using ValueType = bool;

 public:
  ToStatus(Wrapped&& thunk)
      : wrapped_(std::move(thunk)) {
  }

  // Non-copyable
  ToStatus(const ToStatus&) = delete;

  // Movable
  ToStatus(ToStatus&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    wrapped_.Start(this);
  }

 private:
  // IConsumer<InputValueType>

  void Consume(Output<InputValueType> output) noexcept override {
    consumer_->Consume({
        ResultTraits::IsOk(output.value), std::move(output.context)});
  }

  void Cancel(Context ctx) noexcept override {
    consumer_->Cancel(std::move(ctx));
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  Wrapped wrapped_;
  IConsumer<ValueType>* consumer_ = nullptr;
};

}  // namespace thunks

}  // namespace await::futures::lazy
