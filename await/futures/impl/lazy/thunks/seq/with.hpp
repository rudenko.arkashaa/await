#pragma once

#include <await/futures/model/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] With final
    : private IConsumer<typename Wrapped::ValueType> {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  With(Wrapped&& thunk, carry::Context context)
      : wrapped_(std::move(thunk)),
        context_(std::move(context)) {
  }

  // Non-copyable
  With(const With&) = delete;

  // Movable
  With(With&&) = default;

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    wrapped_.Start(this);
  }

 private:
  // IConsumer

  void Consume(Output<ValueType> output) noexcept override {
    output.context.user = std::move(context_);  // Set user context
    consumer_->Consume(std::move(output));
  }

  void Cancel(Context ctx) noexcept override {
    ctx.user = std::move(context_);
    consumer_->Cancel(std::move(ctx));
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  Wrapped wrapped_;
  carry::Context context_;

  IConsumer<ValueType>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy
