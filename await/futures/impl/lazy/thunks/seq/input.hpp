#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/consumers/linked.hpp>

#include <await/cancel/manual.hpp>

namespace await::futures::lazy {

namespace thunks {

// Input for parallel combinator

template <Thunk Wrapped>
class [[nodiscard]] Input final : private IConsumer<typename Wrapped::ValueType> {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  Input(Wrapped&& thunk, size_t index = 0)
      : wrapped_(std::move(thunk)), index_(index) {
  }

  // Non-copyable
  Input(const Input&) = delete;

  // Movable
  Input(Input&&) = default;

  void SetIndex(size_t index) {
    index_ = index;
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    // Inlined allocation
    cancel_state_.Allocate();

    consumer_ = consumers::Link(consumer, cancel_state_.AsSource());

    wrapped_.Start(this);
  }

 private:
  // IConsumer<ValueType>

  cancel::Token CancelToken() override {
    return cancel_state_.MakeToken();
  }

  void Consume(Output<ValueType> output) noexcept override {
    output.context.combinator.index = index_;
    consumer_->Consume(std::move(output));
  }

  void Cancel(Context ctx) noexcept override {
    ctx.combinator.index = index_;
    consumer_->Cancel(std::move(ctx));
  }

 private:
  cancel::ManualState<> cancel_state_;
  Wrapped wrapped_;
  size_t index_;

  consumers::LinkedConsumer<ValueType> consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
