#pragma once

#include <await/futures/model/thunk.hpp>

#include <wheels/core/unit.hpp>

// std::terminate
#include <exception>

namespace await::futures::lazy {

namespace thunks {

class [[nodiscard]] Bomb {
 public:
  using ValueType = wheels::Unit;

 public:
  Bomb() = default;

  // Non-copyable
  Bomb(const Bomb&) = delete;

  // Movable
  Bomb(Bomb&&) = default;

  // Lazy protocol

  void Start(IConsumer<wheels::Unit>*) {
    std::terminate();  // Explode
  }
};

}  // namespace thunks

}  // namespace await::futures::lazy
