#pragma once

#include <await/futures/model/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <typename T>
class [[nodiscard]] Value {
 public:
  using ValueType = T;

 public:
  Value(T value)
      : value_(std::move(value)) {
  }

  // Non-copyable
  Value(const Value&) = delete;

  // Movable
  Value(Value&&) = default;

  // Lazy protocol

  void Start(IConsumer<T>* consumer) {
    consumer->Consume(std::move(value_));
  }

 private:
  T value_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
