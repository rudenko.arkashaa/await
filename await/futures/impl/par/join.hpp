#pragma once

#include <await/futures/impl/value/t/join.hpp>

#include <await/futures/impl/par/detail/done_or_count.hpp>
#include <await/futures/impl/par/detail/maybe.hpp>

#include <cassert>
#include <optional>

namespace await::futures {

namespace combinators {

template <typename InputType>
class JoinCombinator {
 public:
  using Traits = value::JoinTraits<InputType>;

  using OutputType = typename Traits::OutputType;

 public:
  JoinCombinator(size_t inputs)
      : inputs_(inputs) {
  }

  Maybe<OutputType> Combine(InputType input, CombinatorContext) {
    if (Traits::IsOk(input)) {
      if (state_.IncrementAndFetch() == inputs_) {
        return Traits::Ok();  // Last value
      }
    } else {
      if (state_.Done()) {
        return Traits::PropagateError(std::move(input));  // First error
      }
    }

    return {};
  }

  bool CombineCancel() {
    return state_.Done();
  }

  bool IsCompleted() const {
    return true;  // TODO
  }

 private:
  const size_t inputs_;

  detail::DoneOrCountStateMachine state_;
};

}  // namespace combinators

}  // namespace await::futures
