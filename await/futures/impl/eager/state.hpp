#pragma once

#include <await/futures/impl/race/ready.hpp>

#include <await/futures/impl/lazy/thunks/seq/box.hpp>

namespace await::futures::eager {

//////////////////////////////////////////////////////////////////////

template <typename T>
struct IState : virtual lazy::thunks::IBoxedThunk<T> {
  virtual ~IState() = default;

  virtual void SetConsumer(IConsumer<T>* consumer) = 0;

  void Start(IConsumer<T>* consumer) override {
    SetConsumer(consumer);
  }

  // Polling
  virtual race::Ready GetReadyState() const = 0;

  virtual void Drop() = 0;
};

//////////////////////////////////////////////////////////////////////

template <typename T>
using StateRef = refer::Ref<IState<T>>;

}  // namespace await::futures::eager
