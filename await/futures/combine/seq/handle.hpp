#pragma once

#include <await/futures/impl/lazy/thunks/seq/on_cancel.hpp>
#include <await/futures/impl/lazy/thunks/seq/map.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

template <typename F>
struct [[nodiscard]] OnComplete {
  F fun;

  OnComplete(F f)
      : fun(std::move(f)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    using ValueType = ValueOf<Future>;

    auto mapper =
        [h = std::move(fun)](ValueType input) mutable {
          h(static_cast<const ValueType&>(input));
          return input;
        };

    return lazy::thunks::Map{std::move(input), std::move(mapper)};
  }
};

template <typename F>
struct [[nodiscard]] OnCancel {
  F fun;

  OnCancel(F f)
      : fun(std::move(f)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::OnCancel{std::move(input), std::move(fun)};
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

template <typename F>
auto OnComplete(F fun) {
  return pipe::OnComplete(std::move(fun));
}

template <typename F>
auto OnCancel(F fun) {
  return pipe::OnCancel(std::move(fun));
}

}  // namespace await::futures
