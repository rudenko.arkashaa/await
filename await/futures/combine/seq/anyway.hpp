#pragma once

#include <await/futures/combine/seq/handle.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

template <typename Handler>
struct [[nodiscard]] Anyway {
  Handler handler;

  Anyway(Handler h)
      : handler(std::move(h)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    using ValueType = ValueOf<Future>;

    auto value_handler = [handler = handler](const ValueType&) mutable {
      handler();
    };

    return std::move(input) |
           futures::OnComplete(std::move(value_handler)) |
           futures::OnCancel(handler);
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

template <typename Handler>
auto Anyway(Handler handler) {
  return pipe::Anyway{std::move(handler)};
}

}  // namespace await::futures
