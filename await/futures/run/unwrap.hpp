#pragma once

#include <await/futures/model/thunk.hpp>

#include <await/futures/impl/lazy/terminators/unwrap.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace pipe {

struct [[nodiscard]] Unwrap {
  template <SomeFuture Future>
  auto Pipe(Future input) {
    lazy::terminators::UnwrapConsumer<ValueOf<Future>> consumer;
    input.Start(&consumer);
    return consumer.ExpectValue();
  }
};

}  // namespace pipe

//////////////////////////////////////////////////////////////////////

inline auto Unwrap() {
  return pipe::Unwrap();
}

}  // namespace await::futures
